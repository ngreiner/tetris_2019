;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; tetris.asm
;;
;; Author: Nathan GREINER
;;         Salma ED-DAHABI
;;
;; Created on: 6 November 2019
;;
;; Notes:
;;    Adapted from template (CS-208, EPFL, Moodle).
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; game state memory location
.equ T_X, 0x1000                  ; falling tetromino position on x
.equ T_Y, 0x1004                  ; falling tetromino position on y
.equ T_type, 0x1008               ; falling tetromino type
.equ T_orientation, 0x100C        ; falling tetromino orientation
.equ SCORE,  0x1010               ; score
.equ GSA, 0x1014                  ; Game State Array starting address
.equ SEVEN_SEGS, 0x1198           ; 7-segment display addresses
.equ LEDS, 0x2000                 ; LED address
.equ RANDOM_NUM, 0x2010           ; Random number generator address
.equ BUTTONS, 0x2030              ; Buttons addresses

;; type enumeration
.equ C, 0x00
.equ B, 0x01
.equ T, 0x02
.equ S, 0x03
.equ L, 0x04

;; GSA type
.equ NOTHING, 0x0
.equ PLACED, 0x1
.equ FALLING, 0x2

;; orientation enumeration
.equ N, 0
.equ E, 1
.equ So, 2
.equ W, 3
.equ ORIENTATION_END, 4

;; collision boundaries
.equ COL_X, 4
.equ COL_Y, 3

;; Rotation enumeration
.equ CLOCKWISE, 0
.equ COUNTERCLOCKWISE, 1

;; Button enumeration
.equ moveL, 0x01
.equ rotL, 0x02
.equ reset, 0x04
.equ rotR, 0x08
.equ moveR, 0x10
.equ moveD, 0x20

;; Collision return ENUM
.equ W_COL, 0
.equ E_COL, 1
.equ So_COL, 2
.equ OVERLAP, 3
.equ NONE, 4

;; start location
.equ START_X, 6
.equ START_Y, 1

;; game rate of tetromino falling down (in terms of game loop iteration)
.equ RATE, 5

;; standard limits
.equ X_LIMIT, 12
.equ Y_LIMIT, 8


;;;;;;;;;;;;;;; PROCEDURES ;;;;;;;;;;;;;;;

; BEGIN:main

    call wait

    init_main:
    addi sp, zero, 0x2000
    call reset_game
    addi s0, zero, RATE
    addi s1, zero, reset
    addi s2, zero, 0
    addi s4, zero, NONE

    move_rotate_main:
    call draw_gsa
    addi a0, zero, 0
    call draw_tetromino
    call wait
    call get_input
    add s3, v0, zero
    beq s3, zero, skip_act_main
    add a0, s3, zero
    call act
    skip_act_main:
    addi a0, zero, FALLING
    call draw_tetromino
    bne s3, s1, inc_RATE_main
    addi s2, zero, 0
    br move_rotate_main
    inc_RATE_main:
    addi s2, s2, 1
    bne s2, s0, move_rotate_main

    ;; Move tetromino down.
    addi a0, zero, 0
    call draw_tetromino
    addi a0, zero, moveD
    call act
    bne v0, zero, failed_move_down_main
    addi a0, zero, FALLING
    call draw_tetromino
    addi s2, zero, 0
    br move_rotate_main
    failed_move_down_main:
    addi a0, zero, PLACED
    call draw_tetromino

    ;; Detect full lines.
    loop_detect_main:
    call detect_full_line
    cmpeqi t0, v0, 8
    bne t0, zero, gen_tet_main
    addi a0, v0, 0
    call remove_full_line
    call increment_score
    call display_score
    br loop_detect_main

    ;; Generate new tetromino.
    gen_tet_main:
    call generate_tetromino
    addi a0, zero, OVERLAP
    call detect_collision
    bne v0, s4, init_main
    addi a0, zero, FALLING
    call draw_tetromino
    addi s2, zero, 0
    br move_rotate_main

; END:main 



; BEGIN:clear_leds
clear_leds:
    addi t0, zero, 0
    addi t1, zero, 4
    addi t2, zero, 8
    stw zero, LEDS(t0)
    stw zero, LEDS(t1)
    stw zero, LEDS(t2)
    ret
; END:clear_leds


; BEGIN:set_pixel
set_pixel:
    addi t0, zero, 0
    addi t1, zero, 4
    blt a0, t1, bit_index_set_pixel
    addi t0, t0, 4
    addi a0, a0, -4
    blt a0, t1, bit_index_set_pixel
    addi t0, t0, 4
    addi a0, a0, -4
    bit_index_set_pixel:
    slli t1, a0, 3
    add t1, t1, a1
    addi t3, zero, 1
    sll t3, t3, t1
    ;; load and store updated word
    ldw t2, LEDS(t0)
    or t2, t2, t3
    stw t2, LEDS(t0)
    ret
; END:set_pixel


; BEGIN:wait
wait:
    addi t0, zero, 1
    slli t0, t0, 20
    loop_wait:
    addi t0, t0, -1
    bne t0, zero, loop_wait
    ret
; END:wait


; BEGIN:in_gsa
in_gsa:
    addi v0, zero, 1
    blt a0, zero, end_in_gsa
    blt a1, zero, end_in_gsa
    addi t0, zero, 12
    bge a0, t0, end_in_gsa
    addi t0, zero, 8
    bge a1, t0, end_in_gsa
    addi v0, zero, 0
    end_in_gsa:
    ret
; END:in_gsa


; BEGIN:get_gsa
get_gsa:
    slli t0, a0, 3
    add t0, t0, a1
    slli t0, t0, 2
    ldw v0, GSA(t0)
    ret
; END:get_gsa


; BEGIN:set_gsa
set_gsa:
    slli t0, a0, 3
    add t0, t0, a1
    slli t0, t0, 2
    stw a2, GSA(t0)
    ret
; END:set_gsa


; BEGIN:draw_gsa
draw_gsa:
    addi t0, zero, 380  ; offset GSA
    addi t1, zero, 8    ; offset LEDs
    outer_loop_draw_gsa:
    addi t2, zero, 32  ; counter inner-loop
    addi t3, zero, 0   ; word to store in LEDs
    inner_loop_draw_gsa:
    ldw t4, GSA(t0)
    cmpne t4, t4, zero
    add t3, t3, t4
    roli t3, t3, 1
    addi t2, t2, -1
    addi t0, t0, -4
    bne t2, zero, inner_loop_draw_gsa
    roli t3, t3, 31
    stw t3, LEDS(t1)
    addi t1, t1, -4
    bge t1, zero, outer_loop_draw_gsa
    ret
; END:draw_gsa


; BEGIN:draw_tetromino
draw_tetromino:
    addi sp, sp, -32
    stw ra, 0(sp)
    stw s0, 4(sp)
    stw s1, 8(sp)
    stw s2, 12(sp)
    stw s3, 16(sp)
    stw s4, 20(sp)
    stw s5, 24(sp)
    stw s6, 28(sp)
    ;; draw anchor point of tetromino
    add s0, a0, zero
    ldw s1, T_X(zero)
    ldw s2, T_Y(zero)
    add a0, s1, zero
    add a1, s2, zero
    add a2, s0, zero
    call set_gsa
    ;; find offset addresses
    ldw t0, T_type(zero)
    ldw t1, T_orientation(zero)
    slli t0, t0, 2
    add t0, t0, t1
    slli t0, t0, 2
    ldw s3, DRAW_Ax(t0)
    ldw s4, DRAW_Ay(t0)
    ;; draw offset points of tetromino
    addi s5, zero, 0
    addi s6, zero, 12
    loop_draw_tetromino:
    add t0, s3, s5
    ldw t0, 0(t0)
    add a0, s1, t0
    add t0, s4, s5
    ldw t0, 0(t0)
    add a1, s2, t0
    add a2, s0, zero
    call set_gsa
    addi s5, s5, 4
    bne s5, s6, loop_draw_tetromino
    ;; the end
    ldw ra, 0(sp)
    ldw s0, 4(sp)
    ldw s1, 8(sp)
    ldw s2, 12(sp)
    ldw s3, 16(sp)
    ldw s4, 20(sp)
    ldw s5, 24(sp)
    ldw s6, 28(sp)
    addi sp, sp, 32
    ret
; END:draw_tetromino


; BEGIN:generate_tetromino
generate_tetromino:
    ldw t0, RANDOM_NUM(zero)
    andi t0, t0, 7
    cmplti t1, t0, 5
    beq t1, zero, generate_tetromino
    stw t0, T_type(zero)
    addi t0, zero, START_X
    stw t0, T_X(zero)
    addi t0, zero, START_Y
    stw t0, T_Y(zero)
    addi t0, zero, N
    stw t0, T_orientation(zero)
    ret
; END:generate_tetromino


; BEGIN:detect_collision
detect_collision:
    addi sp, sp, -36
    stw ra, 0(sp)
    stw s0, 4(sp)
    stw s1, 8(sp)
    stw s2, 12(sp)
    stw s3, 16(sp)
    stw s4, 20(sp)
    stw s5, 24(sp)
    stw s6, 28(sp)
    stw s7, 32(sp)
    add s0, a0, zero
    ;; get anchor point of tetromino
    ldw s1, T_X(zero)
    ldw s2, T_Y(zero)
    ;; find offset addresses
    ldw t0, T_type(zero)
    ldw t1, T_orientation(zero)
    slli t0, t0, 2
    add t0, t0, t1
    slli t0, t0, 2
    ldw s3, DRAW_Ax(t0)
    ldw s4, DRAW_Ay(t0)
    ;; prepare address offset for collision detection
    addi s5, zero, 8    
    ;; check collision type
    addi t0, zero, W_COL
    beq s0, t0, W_COL_detect_collision
    addi t0, zero, E_COL
    beq s0, t0, E_COL_detect_collision
    addi t0, zero, So_COL
    beq s0, t0, So_COL_detect_collision
    addi t0, zero, OVERLAP
    beq s0, t0, OVERLAP_detect_collision
    W_COL_detect_collision:
    addi s1, s1, -1
    br OVERLAP_detect_collision
    E_COL_detect_collision:
    addi s1, s1, 1
    br OVERLAP_detect_collision
    So_COL_detect_collision:
    addi s2, s2, 1
    OVERLAP_detect_collision:
    ;; check if anchor point is in GSA
    add a0, s1, zero
    add a1, s2, zero
    call in_gsa
    bne v0, zero, exit_true_detect_collision
    ;; check if GSA at anchor point is occupied
    add a0, s1, zero
    add a1, s2, zero
    call get_gsa
    bne v0, zero, exit_true_detect_collision
    loop_OVERLAP_detect_collision:
    ;; load and add x-offset
    add t0, s3, s5
    ldw t0, 0(t0)
    add s6, s1, t0
    ;; load and add y-offset
    add t0, s4, s5
    ldw t0, 0(t0)
    add s7, s2, t0
    ;; check if (x,y) is in GSA
    add a0, s6, zero
    add a1, s7, zero
    call in_gsa
    bne v0, zero, exit_true_detect_collision
    ;; check if GSA(x,y) is occupied
    add a0, s6, zero
    add a1, s7, zero
    call get_gsa
    bne v0, zero, exit_true_detect_collision
    ;; decrement address offset and exit if applicable
    addi s5, s5, -4
    blt s5, zero, exit_false_detect_collision
    br loop_OVERLAP_detect_collision
    exit_true_detect_collision:
    add v0, zero, s0
    br exit_detect_collision
    exit_false_detect_collision:
    addi v0, zero, NONE
    exit_detect_collision:
    ldw ra, 0(sp)
    ldw s0, 4(sp)
    ldw s1, 8(sp)
    ldw s2, 12(sp)
    ldw s3, 16(sp)
    ldw s4, 20(sp)
    ldw s5, 24(sp)
    ldw s6, 28(sp)
    ldw s7, 32(sp)
    addi sp, sp, 36
    ret
; END:detect_collision


; BEGIN:rotate_tetromino 
rotate_tetromino:
    addi t0, zero, rotL
    addi t1, zero, 1
    bne a0, t0, do_rotate_tetromino
    addi t1, zero, -1
    do_rotate_tetromino:
    ldw t0, T_orientation(zero)
    add t0, t0, t1
    andi t0, t0, 3
    stw t0, T_orientation(zero)
    ret
; END:rotate_tetromino


; BEGIN:act 
act:
    addi sp, sp, -20
    stw ra, 0(sp)
    stw s0, 4(sp)
    stw s1, 8(sp)
    stw s2, 12(sp)
    stw s3, 16(sp)
    addi s0, zero, NONE ; store NONE in s0
    addi t0, zero, moveL
    beq a0, t0, movL_act
    addi t0, zero, moveR
    beq a0, t0, movR_act
    addi t0, zero, moveD
    beq a0, t0, movD_act
    addi t0, zero, reset
    beq a0, t0, reset_act
    addi s1, zero, rotL ; suppose rotation is rightwards, prepare reverse rotation
    bne a0, s1, rot_act ; rectify if needed
    addi s1, zero, rotR
    rot_act:
    call rotate_tetromino
    addi a0, zero, OVERLAP
    call detect_collision
    bne v0, s0, shift_direction_act
    br success_act
    shift_direction_act:
    addi s2, zero, -1
    ldw s3, T_X(zero)
    addi t1, zero, 6
    bge s3, t1, shift1_act
    addi s2, zero, 1
    shift1_act:
    add s3, s3, s2
    stw s3, T_X(zero)
    addi a0, zero, OVERLAP
    call detect_collision
    bne v0, s0, shift2_act
    br success_act
    shift2_act:
    add s3, s3, s2
    stw s3, T_X(zero)
    addi a0, zero, OVERLAP
    call detect_collision
    beq v0, s0, success_act
    addi t0, zero, -1
    xor s2, s2, t0
    add s3, s3, s2
    add s3, s3, s2
    add a0, s1, zero
    call rotate_tetromino
    br failure_act
    movL_act:
    addi a0, zero, W_COL
    call detect_collision
    bne v0, s0, failure_act
    ldw t0, T_X(zero)
    addi t0, t0, -1
    stw t0, T_X(zero)
    br success_act
    movR_act:
    addi a0, zero, E_COL
    call detect_collision
    bne v0, s0, failure_act
    ldw t0, T_X(zero)
    addi t0, t0, 1
    stw t0, T_X(zero)
    br success_act
    movD_act:
    addi a0, zero, So_COL
    call detect_collision
    bne v0, s0, failure_act
    ldw t0, T_Y(zero)
    addi t0, t0, 1
    stw t0, T_Y(zero)
    br success_act
    reset_act:
    call reset_game
    br success_act
    failure_act:
    addi v0, zero, 1
    br end_act
    success_act:
    addi v0, zero, 0
    end_act:
    ldw ra, 0(sp)
    ldw s0, 4(sp)
    ldw s1, 8(sp)
    ldw s2, 12(sp)
    ldw s3, 16(sp)
    addi sp, sp, 20
    ret
; END:act


; BEGIN:get_input
get_input:
    ;; load edgecapture
    addi t0, zero, 4
    ldw t1, BUTTONS(t0)
    ;; find pushed button if any
    addi v0, zero, moveL
    andi t2, t1, 0x0001
    bne t2, zero, end_get_input
    addi v0, zero, rotL
    andi t2, t1, 0x0002
    bne t2, zero, end_get_input
    addi v0, zero, reset
    andi t2, t1, 0x0004
    bne t2, zero, end_get_input
    addi v0, zero, rotR
    andi t2, t1, 0x0008
    bne t2, zero, end_get_input
    addi v0, zero, moveR
    andi t2, t1, 0x0010
    bne t2, zero, end_get_input
    ;; if none, reassign v0
    add v0, zero, zero
    end_get_input:
    addi t3, zero, -1
    slli t3, t3, 5
    and t1, t1, t3
    stw t1, BUTTONS(t0)
    ret
; END:get_input


; BEGIN:detect_full_line
detect_full_line:
    addi sp, sp, -28
    stw ra, 0(sp)
    stw s0, 4(sp)
    stw s1, 8(sp)
    stw s2, 12(sp)
    stw s3, 16(sp)
    stw s4, 20(sp)
    stw s5, 24(sp)
    addi s0, zero, 0
    addi s2, zero, 8
    addi s3, zero, 12
    loop_y_detect_full_line:
    addi s1, zero, 0
    loop_x_detect_full_line:
    addi a0, s1, 0
    addi a1, s0, 0
    call get_gsa
    beq v0, zero, inc_y_detect_full_line
    addi s1, s1, 1
    bne s1, s3, loop_x_detect_full_line
    add v0, s0, zero
    br end_detect_full_line
    inc_y_detect_full_line:
    addi s0, s0, 1
    bne s0, s2, loop_y_detect_full_line
    add v0, s0, zero
    end_detect_full_line:
    ldw ra, 0(sp)
    ldw s0, 4(sp)
    ldw s1, 8(sp)
    ldw s2, 12(sp)
    ldw s3, 16(sp)
    ldw s4, 20(sp)
    ldw s5, 24(sp)
    addi sp, sp, 28
    ret
; END:detect_full_line


; BEGIN:remove_full_line
remove_full_line:
    addi sp, sp, -28
    stw ra, 0(sp)
    stw s0, 4(sp)
    stw s1, 8(sp)
    stw s2, 12(sp)
    stw s3, 16(sp)
    stw s4, 20(sp)
    stw s5, 24(sp)
    addi s0, a0, 0
    ;; prepare registers for line blinking
    addi t0, zero, 1
    sll t0, t0, s0
    slli t1, t0, 8
    slli t2, t1, 8
    slli s1, t2, 8
    add s1, s1, t2
    add s1, s1, t1
    add s1, s1, t0
    addi t0, zero, -1
    xor s2, s1, t0
    ;; make line blink
    addi t0, zero, 0
    addi t1, zero, 4
    addi t2, zero, 8
    ldw s3, LEDS(t0)
    ldw s4, LEDS(t1)
    ldw s5, LEDS(t2)
    and t3, s3, s2
    and t4, s4, s2
    and t5, s5, s2
    stw t3, LEDS(t0)
    stw t4, LEDS(t1)
    stw t5, LEDS(t2)  ;; line is off
    call wait
    addi t0, zero, 0
    addi t1, zero, 4
    addi t2, zero, 8
    stw s3, LEDS(t0)
    stw s4, LEDS(t1)
    stw s5, LEDS(t2)  ;; line is on
    call wait
    addi t0, zero, 0
    addi t1, zero, 4
    addi t2, zero, 8
    and t3, s3, s2
    and t4, s4, s2
    and t5, s5, s2
    stw t3, LEDS(t0)
    stw t4, LEDS(t1)
    stw t5, LEDS(t2) ;; line is off
    call wait
    addi t0, zero, 0
    addi t1, zero, 4
    addi t2, zero, 8
    stw s3, LEDS(t0)
    stw s4, LEDS(t1)
    stw s5, LEDS(t2)  ;; line is on
    call wait
    addi t0, zero, 0
    addi t1, zero, 4
    addi t2, zero, 8
    and t3, s3, s2
    and t4, s4, s2
    and t5, s5, s2
    stw t3, LEDS(t0)
    stw t4, LEDS(t1)
    stw t5, LEDS(t2) ;; line is off
    call wait
    ;; push down lines of GSA
    beq s0, zero, skip_loop_y_remove_full_line
    loop_y_remove_full_line:
    addi s1, zero, 11
    loop_x_remove_full_line:
    addi a0, s1, 0
    addi a1, s0, -1
    call get_gsa
    addi a0, s1, 0
    addi a1, s0, 0
    addi a2, v0, 0
    call set_gsa
    addi s1, s1, -1
    bge s1, zero, loop_x_remove_full_line
    addi s0, s0, -1
    bne s0, zero, loop_y_remove_full_line
    ;; set first line of GSA to NOTHING
    skip_loop_y_remove_full_line:
    addi s1, zero, 11
    loop_first_line_remove_full_line:
    addi a0, s1, 0
    addi a1, zero, 0
    addi a2, zero, NOTHING
    call set_gsa
    addi s1, s1, -1
    bge s1, zero, loop_first_line_remove_full_line
    ;; the end
    ldw ra, 0(sp)
    ldw s0, 4(sp)
    ldw s1, 8(sp)
    ldw s2, 12(sp)
    ldw s3, 16(sp)
    ldw s4, 20(sp)
    ldw s5, 24(sp)
    addi sp, sp, 28
    ret
; END:remove_full_line


; BEGIN:increment_score
increment_score:
    addi t0, zero, 9999
    ldw t1, SCORE(zero)
    bge t1, t0, end_increment_score
    addi t1, t1, 1
    end_increment_score:
    stw t1, SCORE(zero)
    ret
; END:increment_score


; BEGIN:display_score
display_score:
    ldw t0, SCORE(zero)
    addi t1, zero, 10
    ; digit1
    addi t2, t0, 0
    display_score_fetch_units:
    bltu t2, t1, display_score_digit1
    addi t2, t2, -10
    br display_score_fetch_units
    display_score_digit1:
    addi t3, zero, 0x000C
    slli t4, t2, 2
    ldw t4, font_data(t4)
    stw t4, SEVEN_SEGS(t3)
    ; digit2
    sub t2, t0, t2
    addi t3, zero, 0
    addi t4, zero, 0
    beq t2, t4, display_score_digit2
    addi t3, zero, 1
    addi t4, zero, 10
    beq t2, t4, display_score_digit2
    addi t3, zero, 2
    addi t4, zero, 20
    beq t2, t4, display_score_digit2
    addi t3, zero, 3
    addi t4, zero, 30
    beq t2, t4, display_score_digit2
    addi t3, zero, 4
    addi t4, zero, 40
    beq t2, t4, display_score_digit2
    addi t3, zero, 5
    addi t4, zero, 50
    beq t2, t4, display_score_digit2
    addi t3, zero, 6
    addi t4, zero, 60
    beq t2, t4, display_score_digit2
    addi t3, zero, 7
    addi t4, zero, 70
    beq t2, t4, display_score_digit2
    addi t3, zero, 8
    addi t4, zero, 80
    beq t2, t4, display_score_digit2
    addi t3, zero, 9
    display_score_digit2:
    addi t4, zero, 0x0008
    slli t5, t3, 2
    ldw t5, font_data(t5)
    stw t5, SEVEN_SEGS(t4)
    ; digit3
    addi t0, zero, 0x0004
    ldw t1, font_data(zero)
    stw t1, SEVEN_SEGS(t0)
    ; digit4
    stw t1, SEVEN_SEGS(zero)
    ret
; END:display_score


; BEGIN:reset_game
reset_game:
    addi sp, sp, -4
    stw ra, 0(sp)
    stw zero, SCORE(zero)
    call display_score
    addi t0, zero, 384
    loop_GSA_reset_game:
    addi t0, t0, -4
    stw zero, GSA(t0)
    bne t0, zero, loop_GSA_reset_game
    call generate_tetromino
    addi a0, zero, FALLING
    call draw_tetromino
    call draw_gsa
    ldw ra, 0(sp)
    addi sp, sp, 4
    ret
; END:reset_game


;;;;;;;;;;;;;;;;;;;;;; MEMORY WORDS ;;;;;;;;;;;;;;;;;;;;;;;
;; digit encoding for SEVEN_SEGS
font_data:
.word 0xFC  ; 0
.word 0x60  ; 1
.word 0xDA  ; 2
.word 0xF2  ; 3
.word 0x66  ; 4
.word 0xB6  ; 5
.word 0xBE  ; 6
.word 0xE0  ; 7
.word 0xFE  ; 8
.word 0xF6  ; 9

C_N_X:
.word 0x00
.word 0xFFFFFFFF
.word 0xFFFFFFFF

C_N_Y:
.word 0xFFFFFFFF
.word 0x00
.word 0xFFFFFFFF

C_E_X:
.word 0x01
.word 0x00
.word 0x01

C_E_Y:
.word 0x00
.word 0xFFFFFFFF
.word 0xFFFFFFFF

C_So_X:
.word 0x01
.word 0x00
.word 0x01

C_So_Y:
.word 0x00
.word 0x01
.word 0x01

C_W_X:
.word 0xFFFFFFFF
.word 0x00
.word 0xFFFFFFFF

C_W_Y:
.word 0x00
.word 0x01
.word 0x01

B_N_X:
.word 0xFFFFFFFF
.word 0x01
.word 0x02

B_N_Y:
.word 0x00
.word 0x00
.word 0x00

B_E_X:
.word 0x00
.word 0x00
.word 0x00

B_E_Y:
.word 0xFFFFFFFF
.word 0x01
.word 0x02

B_So_X:
.word 0xFFFFFFFE
.word 0xFFFFFFFF
.word 0x01

B_So_Y:
.word 0x00
.word 0x00
.word 0x00

B_W_X:
.word 0x00
.word 0x00
.word 0x00

B_W_Y:
.word 0xFFFFFFFE
.word 0xFFFFFFFF
.word 0x01

T_N_X:
.word 0xFFFFFFFF
.word 0x00
.word 0x01

T_N_Y:
.word 0x00
.word 0xFFFFFFFF
.word 0x00

T_E_X:
.word 0x00
.word 0x01
.word 0x00

T_E_Y:
.word 0xFFFFFFFF
.word 0x00
.word 0x01

T_So_X:
.word 0xFFFFFFFF
.word 0x00
.word 0x01

T_So_Y:
.word 0x00
.word 0x01
.word 0x00

T_W_X:
.word 0x00
.word 0xFFFFFFFF
.word 0x00

T_W_Y:
.word 0xFFFFFFFF
.word 0x00
.word 0x01

S_N_X:
.word 0xFFFFFFFF
.word 0x00
.word 0x01

S_N_Y:
.word 0x00
.word 0xFFFFFFFF
.word 0xFFFFFFFF

S_E_X:
.word 0x00
.word 0x01
.word 0x01

S_E_Y:
.word 0xFFFFFFFF
.word 0x00
.word 0x01

S_So_X:
.word 0x01
.word 0x00
.word 0xFFFFFFFF

S_So_Y:
.word 0x00
.word 0x01
.word 0x01

S_W_X:
.word 0x00
.word 0xFFFFFFFF
.word 0xFFFFFFFF

S_W_Y:
.word 0x01
.word 0x00
.word 0xFFFFFFFF

L_N_X:
.word 0xFFFFFFFF
.word 0x01
.word 0x01

L_N_Y:
.word 0x00
.word 0x00
.word 0xFFFFFFFF

L_E_X:
.word 0x00
.word 0x00
.word 0x01

L_E_Y:
.word 0xFFFFFFFF
.word 0x01
.word 0x01

L_So_X:
.word 0xFFFFFFFF
.word 0x01
.word 0xFFFFFFFF

L_So_Y:
.word 0x00
.word 0x00
.word 0x01

L_W_X:
.word 0x00
.word 0x00
.word 0xFFFFFFFF

L_W_Y:
.word 0x01
.word 0xFFFFFFFF
.word 0xFFFFFFFF

DRAW_Ax:  ;; address of shape arrays, x axis
.word C_N_X
.word C_E_X
.word C_So_X
.word C_W_X
.word B_N_X
.word B_E_X
.word B_So_X
.word B_W_X
.word T_N_X
.word T_E_X
.word T_So_X
.word T_W_X
.word S_N_X
.word S_E_X
.word S_So_X
.word S_W_X
.word L_N_X
.word L_E_X
.word L_So_X
.word L_W_X

DRAW_Ay:  ;; address of shape arrays, y_axis
.word C_N_Y
.word C_E_Y
.word C_So_Y
.word C_W_Y
.word B_N_Y
.word B_E_Y
.word B_So_Y
.word B_W_Y
.word T_N_Y
.word T_E_Y
.word T_So_Y
.word T_W_Y
.word S_N_Y
.word S_E_Y
.word S_So_Y
.word S_W_Y
.word L_N_Y
.word L_E_Y
.word L_So_Y
.word L_W_Y
